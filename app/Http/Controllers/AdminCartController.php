<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cart;
use DB;

class AdminCartController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware("auth");
        $this->middleware("admin");
    }

    public function index()
    { 
        $carts = DB::table('carts')
            ->select(DB::raw('user_id,name,SUM(total) as total,carts.updated_at as date'))
            ->join('users', 'carts.user_id', '=', 'users.id')
            ->groupBy('user_id')
            ->get();
        
          
        return view("admin.usercart")->with("carts",$carts);
    }

    public function show($id)
    {
        $carts = Cart::with(["user","package"])->where("user_id",$id)->get();

       
        return view("admin.usercartinfo")->with("carts",$carts);
    }
}
