<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Order;
use App\Item;

class AdminOrderController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware("auth");
        $this->middleware("admin");
    }

    public function index()
    {
        $orders = DB::table('orders')
            ->select(DB::raw('orders.id as id,user_id,name,order_randid,orders.updated_at as date, orders.status as status'))
            ->join('users', 'orders.user_id', '=', 'users.id')
            ->get();

        
        return view("admin.userorder")->with("orders",$orders);
    }

    public function show($id)
    {
        $order_id = Order::find($id);
        $orders = Item::with(["package"])->where("order_id",$id)->get();

       
        $data = array(
            "orders" => $orders,
            "order" => $order_id
        );
        return view("admin.userorderinfo")->with($data);
    }

    public function confirm(Request $request)
    {
        $order_id = $request->input("order_id");
        $option = $request->input("options");

        $order = Order::find($order_id);

        
        if($option == 0)
        {
            $order->status = "approved";
        }
        else if($option == 1)
        {
            $order->status = "delivered";
        }

        $order->save();

        return redirect("/user/order")->with("status","Status Changed");
        
    }
}
