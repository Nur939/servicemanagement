<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Package;
use App\Cart;
use App\User;
use Auth;

class CartController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware("auth");
        $this->middleware("user");
    }

    public function add($id)
    {
       $user_id = auth()->user()->id;
       $package_id = $id;

       $package = Package::find($package_id);

       $carts = Cart::with(["user","package"])->get();
    
       foreach($carts as $cart)
       {
            if($cart->user->id == $user_id && $cart->package->id == $package_id)
            {
                $cart->total += $cart->package->price;
                $cart_id = Cart::find($cart->id);
                $cart->total =  $cart->total;
                $cart->save();
                return redirect("/package/show/$id")->with("success","Package added to cart");
                
            }
       }
       
       
       
       
        $cart = new Cart;
        $cart->user_id = $user_id;
        $cart->package_id = $package_id;
        $cart->total = $package->price;
        $cart->save();
         

       return redirect("/package/show/$id")->with("success","Package added to cart");
    }

    public function show()
    {
        $all_carts = Cart::with(["user","package"])->get();
        $id = auth()->user()->id;

        $carts = [];
        foreach($all_carts as $cart)
        {
            if($cart->user_id == $id)
            {
                $carts[] = $cart;
            }
        }
        //$cart = Cart::find($carts->user_id);
       
        
        return view("frontpage.checkout")->with("carts",$carts);
    }

    public function order()
    {
        if(Auth::check())
        {
            $id = auth()->user()->id;
            $carts = Cart::where("user_id",$id)->get();
        }
        else
        {
            $carts = [];
        }
        return view("frontpage.order")->with("carts",$carts);
    }
}
