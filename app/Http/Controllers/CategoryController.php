<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Category;
use App\Package;

class CategoryController extends Controller
{

    public function __construct()
    {
        $this->middleware("auth");
        $this->middleware("admin");
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categories = Category::orderBy("created_at","desc")->paginate(5);
        return view("/system/category/index")->with("categories",$categories);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view("system/category/create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            "name"  =>  "required",
            "category_image" =>  "required"
        ]);
        
        if($request->hasFile("category_image"))
        {
            $fileNameWithExt = $request->file("category_image")->getClientOriginalname();

            $fileName = pathinfo($fileNameWithExt,PATHINFO_FILENAME);

            $extension = $request->file("category_image")->getClientOriginalExtension();

            $fileNameToStore = $fileName."_".time().".".$extension;
            
            $path = $request->file("category_image")->storeAs("public/categories/",$fileNameToStore);
        }

        $cat = new Category;
        $cat->name = $request->input("name");
        $cat->image = $fileNameToStore;
        $cat->save();


            
        return redirect("system/category")->with("success","Category Created");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $category = Category::find($id);
        return view("/system/category/edit")->with("category",$category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[
            "name"  =>  "required"
        ]);
        
        if($request->hasFile("category_image"))
        {
            $fileNameWithExt = $request->file("category_image")->getClientOriginalname();

            $fileName = pathinfo($fileNameWithExt,PATHINFO_FILENAME);

            $extension = $request->file("category_image")->getClientOriginalExtension();

            $fileNameToStore = $fileName."_".time().".".$extension;
            
            $path = $request->file("category_image")->storeAs("public/categories/",$fileNameToStore);
        }

        $cat = Category::find($id);
        $cat->name = $request->input("name");
        if($request->hasFile("category_image"))
        {
            Storage::delete("public/categories/".$cat->image);
            $cat->image = $fileNameToStore;
        }
       
        $cat->save();

        return redirect("/system/category/$cat->id/edit")->with("success","Updated Successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $cat = category::find($id);
        Storage::delete("public/categories/".$cat->image);
        $cat->delete();

        return redirect("/system/category/")->with("error","Deleted Successfully");

    }
}
