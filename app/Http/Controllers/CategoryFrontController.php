<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Cart;
use Auth;

class CategoryFrontController extends Controller
{
    //
    public function index()
    {
        $categories = Category::orderBy("created_at","desc")->paginate(3);
        if(Auth::check())
        {
            $id = auth()->user()->id;
            $carts = Cart::where("user_id",$id)->get();
        }
        else
        {
            $carts = [];
        }

        $data = array(
            "categories"    =>  $categories,
            "carts"         =>  $carts
        );

        return view("frontpage.categories")->with($data);
    }

    public function show($id)
    {
        $services = Category::find($id)->services()->paginate(3);
        $category = Category::find($id);
        if(Auth::check())
        {
            $id = auth()->user()->id;
            $carts = Cart::where("user_id",$id)->get();
        }
        else
        {
            $carts = [];
        }
        $data = array(
            "category"  =>  $category,
            "services"  =>  $services,
            "carts"     =>  $carts
        );
        return view("frontpage.servicesshow")->with($data);
    }
}
