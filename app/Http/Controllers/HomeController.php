<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Order;
use App\Item;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware("user");
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function orders()
    {
        $user_id = auth()->user()->id;

        $orders = DB::table('orders')
            ->select(DB::raw('orders.id as id,user_id,name,order_randid,orders.updated_at as date, orders.status as status'))
            ->join('users', 'orders.user_id', '=', 'users.id')
            ->where("user_id",$user_id)
            ->get();

        
        return view("user.userorder")->with("orders",$orders);
    }

    public function confirm(Request $request)
    {
        $order_id = $request->input("order_id");
        $option = $request->input("options");

        $order = Order::find($order_id);

        
        if($option == 0)
        {
            $order->status = "confirm";
        }

        $order->save();

        return redirect("/orders")->with("status","Status Changed");
    }

    public function show($id)
    {
        $order_id = Order::find($id);
        $orders = Item::with(["package"])->where("order_id",$id)->get();

       
        $data = array(
            "orders" => $orders,
            "order" => $order_id
        );
        return view("user.userorderinfo")->with($data);
    }
}
