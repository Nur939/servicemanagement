<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Category;
use App\Service;
use App\Package;
use App\Cart;
use Auth;

class MainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categories = Category::orderBy("created_at","desc")->paginate(4);
        $services = Service::orderBy("created_at","desc")->paginate(4);
        $packages = Package::orderBy("created_at","desc")->get();
        $carts = [];

        if(Auth::check())
        {
            $id = auth()->user()->id;
            $carts = Cart::where("user_id",$id)->get();
        }
        else
        {
            $carts = [];
        }
        
        $data = array(
            "categories" => $categories,
            "services"  =>  $services,
            "packages"  =>  $packages,
            "carts" =>  $carts
        );
        return view("frontpage.index")->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
