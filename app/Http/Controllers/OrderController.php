<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cart;
use App\Order;
use App\Item;

class OrderController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware("auth");
        $this->middleware("user");
    }

    public function index()
    {
        $user_id = auth()->user()->id;
        
        $carts = Cart::where("user_id",$user_id)->get();

        if(count($carts) > 0)
        {
            $order = new Order;
            $order->user_id = $user_id;
            $order->status = "pending";
            $length = 15;
            $order->order_randid = substr(str_shuffle(rand().time()."abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
            $order->save();

            foreach($carts as $cart)
            {
                $item = new Item;
                $item->order_id = $order->id;
                $item->package_id = $cart->package_id;
                $item->save();

                if($item->id > 0)
                {
                    $carts = Cart::where("package_id",$cart->package->id)->get();

                    if(count($carts) > 0)
                    {
                    foreach($carts as $cart)
                    {
                       $new_cart = Cart::find($cart->id);
                       $new_cart->delete();
                    }
                }

            }
        }
        
            

            return view("frontpage.order");
        }    
    }
}
