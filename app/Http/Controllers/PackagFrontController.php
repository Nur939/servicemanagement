<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Package;
use App\Cart;
use Auth;

class PackagFrontController extends Controller
{
    //
    public function index()
    {
        $packages = Package::orderBy("created_at","desc")->paginate(3);
        if(Auth::check())
        {
            $id = auth()->user()->id;
            $carts = Cart::where("user_id",$id)->get();
        }
        else
        {
            $carts = [];
        }

        $data = array(
            "packages"    =>  $packages,
            "carts"         =>  $carts
        );

        return view("frontpage.products")->with($data);
    }

    public function show($id)
    {
        $package = Package::find($id);
        $packages = Package::all();
        $ratings = Package::find($id)->ratings;
        $rate = 0;
        $sum = 0;
        $count = 0;
        
        if(count($ratings) > 0)
        {
            foreach($ratings as $rating)
            {
                $count++;
                $sum += (int)$rating->rating; 
            }
            $avg_rating =  $sum / $count; 
        }
        else
        {
            $avg_rating = 0;
        }

        
        
        if(Auth::check())
        {
            $id = auth()->user()->id;
            $carts = Cart::where("user_id",$id)->get();
            

            if(count($ratings) > 0)
            {
                foreach($ratings as $rating)
                {
                    if($rating->user_id == $id)
                    {
                        $rate = $rating->rating;
                    }
                    $sum += (int)$rate;
                }
                
            }
            else
            {
                $rate = 0;
            }    
        }
        else
        {
            $carts = [];
            $rate = 0;
        }

        
        $data = array(
            "package"   => $package,
            "packages"  =>  $packages,
            "service"   =>  $package->service,
            "category"  =>  $package->service->category,
            "carts"     =>  $carts,
            "rate"      =>  $rate,
            "avg_rating"    =>  $avg_rating,
            "count"     =>  $count
        );

        return view("frontpage.packagedetails")->with($data);
    }
}
