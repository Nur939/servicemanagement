<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Service;
use App\Package;

class PackagesController extends Controller
{
    public function __construct()
    {
        $this->middleware("auth");
        $this->middleware("admin");
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $packages = Package::orderBy("created_at","desc")->paginate(5);

        return view("/system/package/index")->with("packages",$packages);

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $select = Service::all();

        $services = [];

        foreach($select as $data)
        {
            $services[$data->id] = $data->name;
        }
        return view("system/package/create")->with("services",$services);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            "Name"  =>  "required",
            "service"  =>  "required",
            "price"         =>  "required",
            "Package_Image" =>  "required",
            "description"   =>  "required",
        ]);

        if($request->hasFile("Package_Image"))
        {
            $fileNameWithExt = $request->file("Package_Image")->getClientOriginalname();

            $fileName = pathinfo($fileNameWithExt,PATHINFO_FILENAME);

            $extension = $request->file("Package_Image")->getClientOriginalExtension();

            $fileNameToStore = $fileName."_".time().".".$extension;
            
            $path = $request->file("Package_Image")->storeAs("public/packages/",$fileNameToStore);
        }

        $pck = new package;
        $pck->name = $request->input("Name");
        $pck->service_id = $request->input("service");
        $pck->image = $fileNameToStore;
        $pck->description = $request->input("description");
        $pck->price = $request->input("price");
        $pck->save();
        
            
        return redirect("system/package")->with("success","Package Created");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $package = Package::find($id);
        return view("/system/package/show")->with("package",$package);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $package = Package::find($id);
        $services = Service::all();

        $select = [];

        foreach($services as $service)
        {
            $select[$service->id] = $service->name;
        }

        $data = array(
            "package"   => $package,
            "select"    => $select
        );
        return view("/system/package/edit")->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[
            "name"  =>  "required",
            "service"  =>  "required",
            "price"    =>   "required",
            "description"   =>  "required"
        ]);

        if($request->hasFile("package_image"))
        {
            $fileNameWithExt = $request->file("package_image")->getClientOriginalname();

            $fileName = pathinfo($fileNameWithExt,PATHINFO_FILENAME);

            $extension = $request->file("package_image")->getClientOriginalExtension();

            $fileNameToStore = $fileName."_".time().".".$extension;
            
            $path = $request->file("package_image")->storeAs("public/packages/",$fileNameToStore);
        }


        $pck = Package::find($id);
        $pck->name = $request->input("name");
        $pck->service_id = $request->input("service");
        $pck->description = $request->input("description");
        $pck->price = $request->input("price");
        if($request->hasFile("package_image"))
        {
            Storage::delete("public/packages/".$pck->image);
            $pck->image = $fileNameToStore;
        }
        
        $pck->save();
            
        return redirect("system/package")->with("success","Package Updated");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $pck = Package::find($id);
        Storage::delete("public/packages/".$pck->image);
        $pck->delete();

        return redirect("/system/package/")->with("error","Deleted Successfully");
    }
}
