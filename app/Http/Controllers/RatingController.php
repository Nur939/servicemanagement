<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rating;
use App\Package;

class RatingController extends Controller
{
    public function __construct()
    {
        $this->middleware("auth");
        $this->middleware("user");
    }

    public function rating(Request $request)
    {

        $this->validate($request,[
            "rating"    => "required"
        ]);

        $rate = $request->input("rating");
        
        $user_id = auth()->user()->id;

        $package_id = $request->input("package");
        $status = 0;

       $pck_ratings = Package::find($package_id)->ratings;
       
       
        if(count($pck_ratings) > 0)
        {
            
            foreach($pck_ratings as $pck_rate)
            {

                if($pck_rate->user_id == $user_id)
                {
                   
                    $rating = Rating::find($pck_rate->id);
                    $rating->rating = $rate;
                    $rating->save();
                    $status = 1;
                    break;
                }
                

               


            }

            if($status == 0)
            {
                $rating = new Rating;

                $rating->rating = $rate;

                $rating->user_id = $user_id;

                $rating->package_id = $package_id;

                $rating->save();
                    
            }    
        }
        else
        {
            $rating = new Rating;

            $rating->rating = $rate;

            $rating->user_id = $user_id;

            $rating->package_id = $package_id;

            $rating->save();
        }    

       

        return redirect("/package/show/$package_id")->with("success","Thank you for your rating");
    }
}
