<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use App\Cart;
use Auth;

class ServiceFrontController extends Controller
{
    //
    public function index()
    {
        $services = Service::orderBy("created_at","desc")->paginate(3);
        if(Auth::check())
        {
            $id = auth()->user()->id;
            $carts = Cart::where("user_id",$id)->get();
        }
        else
        {
            $carts = [];
        }

        $data = array(
            "services"    =>  $services,
            "carts"         =>  $carts
        );

        return view("frontpage.services")->with($data);
    }

    public function show($id)
    {
        $services = Service::find($id)->packages()->paginate(3);
        $service = Service::find($id);
        if(Auth::check())
        {
            $id = auth()->user()->id;
            $carts = Cart::where("user_id",$id)->get();
        }
        else
        {
            $carts = [];
        } 

        $data = array(
            "packages"  =>  $services,
            "service"   =>  $service,
            "carts"     =>  $carts
        );

        return view("frontpage.packagesshow")->with($data);

    }
}
