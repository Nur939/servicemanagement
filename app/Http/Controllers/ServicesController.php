<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Service;
use App\category;

class ServicesController extends Controller
{

    public function __construct()
    {
        $this->middleware("auth");
        $this->middleware("admin");
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $services = Service::orderBy("created_at","desc")->paginate(5);
        return view("/system/service/index")->with("services",$services);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = Category::all();
        $select = [];

        foreach($categories as $category)
        {
            $select[$category->id] = $category->name;
        }
        return view("system/service/create")->with("select",$select);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            "name"  =>  "required",
            "category"  =>  "required",
            "service_image" =>  "required"
        ]);

        if($request->hasFile("service_image"))
        {
            $fileNameWithExt = $request->file("service_image")->getClientOriginalname();

            $fileName = pathinfo($fileNameWithExt,PATHINFO_FILENAME);

            $extension = $request->file("service_image")->getClientOriginalExtension();

            $fileNameToStore = $fileName."_".time().".".$extension;
            
            $path = $request->file("service_image")->storeAs("public/services/",$fileNameToStore);
        }


        $ser = new Service;
        $ser->category_id = $request->input("category");
        $ser->name = $request->input("name");
        $ser->image = $fileNameToStore;
        $ser->save();
            
        return redirect("system/service")->with("success","Service Created");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $service = Service::find($id);
        $categories = Category::all();

        $select = [];

        foreach($categories as $category)
        {
            $select[$category->id] = $category->name;
        }

        $data = array(
            "service"   => $service,
            "select"    => $select
        );
        return view("/system/service/edit")->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[
            "name"  =>  "required",
            "service"  =>  "required"
        ]);

        if($request->hasFile("service_image"))
        {
            $fileNameWithExt = $request->file("service_image")->getClientOriginalname();

            $fileName = pathinfo($fileNameWithExt,PATHINFO_FILENAME);

            $extension = $request->file("service_image")->getClientOriginalExtension();

            $fileNameToStore = $fileName."_".time().".".$extension;
            
            $path = $request->file("service_image")->storeAs("public/services/",$fileNameToStore);
        }


        $ser = Service::find($id);
        $ser->category_id = $request->input("service");
        $ser->name = $request->input("name");
        if($request->hasFile("service_image"))
        {
            Storage::delete("public/services/".$ser->image);
            $ser->image = $fileNameToStore;
        }
        
        $ser->save();
            
        return redirect("system/service")->with("success","Service Updated");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $ser = Service::find($id);
        Storage::delete("public/services/".$ser->image);
        $ser->delete();

        return redirect("/system/service/")->with("error","Deleted Successfully");
    }
}
