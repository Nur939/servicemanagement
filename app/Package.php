<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    //
    public function service()
    {
        return $this->belongsTo("App\Service");
    }

    public function carts()
    {
        return $this->hasMany("App\Cart");
    }

    public function items()
    {
        return $this->hasmany("App\Item");
    }

    public function ratings()
    {
        return $this->hasmany("App\Rating");
    }
}