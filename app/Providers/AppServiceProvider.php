<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

use App\Category;
use App\Service;
use App\Package;
use App\Cart;
use App\Rating;
use Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Schema::defaultStringLength(191);

        $categories = Category::orderBy("created_at","desc")->paginate(4);
        $services = Service::orderBy("created_at","desc")->paginate(4);
        $packages = Package::orderBy("created_at","desc")->get();
        
        $carts = [];

        if(Auth::check())
        {
            $id = auth()->user()->id;
            $carts = Cart::where("user_id",$id)->get();
        }
        else
        {
            $carts = [];
        }
        
        $data = array(
            "categories_menu" => $categories,
            "services_menu"  =>  $services,
            "packages_menu"  =>  $packages,
            "carts" =>  $carts
        );
        view()->share("categories_menu", $categories);
        view()->share("services_menu", $services);
        view()->share("packages_menu", $packages);
        view()->share("carts", $carts);
        // view()->share("avg_rating", $avg_rating);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}