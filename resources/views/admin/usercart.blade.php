@extends("layouts.admin")

@section("page_title","User Carts")

@section("x_title")
    <h1>All Carts Information</h1>
@endsection

@section("content")
   
@if(count($carts) > 0)
<table class = "table table-striped">
  <thead>  
    <tr>
        <th>ID</th>
        <th>User Name</th>
        <th>Date</th>
        <th>Total Price</th>
    </tr>
</thead> 

<tbody>

    @foreach($carts as $cart)
        <tr>
            <td width = "12%">{{$cart->user_id}}</td>
            <td width = "12%"><a href = "/user/cartinfo/{{$cart->user_id}}" class = "text-primary">{{$cart->name}}</a></td>
            <td width = "12%">{{$cart->date }}</td>
            <td width = "12%">{{$cart->total}}</td>
            
        </tr>
    @endforeach
    </tbody> 
    
</table>
@else   
    <p>No Carts Found</p>
@endif

@endsection