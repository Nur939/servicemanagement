@extends("layouts.admin")

@section("page_title","User Cart Information")

@section("x_title")
    @if(count($carts) > 0)
        @foreach($carts as $cart)
           User Name -  {{$cart->user->name}}
            <?php break; ?>
        @endforeach
    @endif
@endsection

@section("content")
   
@if(count($carts) > 0)
<table class = "table table-striped">
  <thead>  
    <tr>
        
        <th>Package Name</th>
        <th>Price</th>
        <th>Total</th>
    </tr>
</thead> 

<tbody>

    @foreach($carts as $cart)
        <tr>
           
            <td width = "12%">{{$cart->package->name}}</td>
            <td width = "12%">{{$cart->package->price}}</td>
            <td width = "12%">{{$cart->total}}</td>
            
        </tr>
    @endforeach
    </tbody> 
    
</table>
@else   
    <p>No Carts Found</p>
@endif

@endsection