@extends("layouts.admin")

@section("page_title","Order Details")

@section("x_title")
    Order ID - {{$order->order_randid}}
@endsection

@section("content")
   
@if(count($orders) > 0)
<table class = "table table-striped">
  <thead>  
    <tr>
        <th>Image</th>
        <th>Package Name</th>
        <th>Price</th>
        <th>Date</th>
    </tr>
</thead> 

<tbody>

    @foreach($orders as $order)
        <tr>
           
            <td width = "12%"><img src = "/storage/packages/{{$order->package->image}}" width = "300px" height = "100px"></td>
            <td width = "12%">{{$order->package->name}}</td>
            <td width = "12%">{{$order->package->price}}</td>
            <td width = "12%">{{$order->created_at}}</td>
            
        </tr>
    @endforeach
    </tbody> 
    
</table>
@else   
    <p>No Carts Found</p>
@endif

@endsection