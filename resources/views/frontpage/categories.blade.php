@extends("layouts.index")


@section("content")

	<!-- BREADCRUMB -->
	<div id="breadcrumb">
		<div class="container">
			<ul class="breadcrumb">
				<li><a href="/">Home</a></li>
				<li class="active">Categories</li>
			</ul>
		</div>
	</div>
	<!-- /BREADCRUMB -->

	<!-- section -->
	<div class="section">
		<!-- container -->
		<div class="container">
			<!-- row -->
			<div class="row">
				

				<!-- MAIN -->
				<div id="main" class="col-md-12">
					{{-- <!-- store top filter -->
					<div class="store-filter clearfix">
						<div class="pull-left">
							<div class="row-filter">
								<a href=""><i class="fa fa-th-large"></i></a>
								<a href="" class="active"><i class="fa fa-bars"></i></a>
							</div>
							<div class="sort-filter">
								<span class="text-uppercase">Sort By:</span>
								<select class="input">
										<option value="0">Position</option>
										<option value="0">Price</option>
										<option value="0">Rating</option>
									</select>
								<a href="" class="main-btn icon-btn"><i class="fa fa-arrow-down"></i></a>
							</div>
						</div>
						
					</div> --}}
					<!-- /store top filter -->

					<!-- STORE -->
					<div id="store">
						<!-- row -->
						<div class="row">
                            <!-- Product Single -->
                            
                            <div class="section-title">
                                <h2 class="title">Categories</h2>
                            </div>
                            @if(count($categories) > 0)
                                @foreach($categories as $category)
                                    <div class="col-md-4 col-sm-6 category">
                                            
                                        <a class="banner banner-1" href="/categories/show/{{$category->id}}">
                                            <img src="/storage/categories/{{$category->image}}" style = "height:250px;" alt="">
                                            
                                            <div class="product-category-title">
                                                    
                                                    <span class="sale">{{$category->name}}</span>
                                                </div>
                                            
                                        </a>
                                    </div>
                                @endforeach	
                            @endif
                            <!-- /banner -->
							<!-- /Product Single -->

							
						</div>
						<!-- /row -->
					</div>
					<!-- /STORE -->

					<!-- store bottom filter -->
					<div class="store-filter clearfix">
						<div class = "pull-right">
							<ul class="store-pages">
									<li class="active">{{$categories->links()}}</li>
								</ul>
						</div>
					</div>
					<!-- /store bottom filter -->
				</div>
				<!-- /MAIN -->
			</div>
			<!-- /row -->
		</div>
		<!-- /container -->
	</div>
	<!-- /section -->

@endsection	

	