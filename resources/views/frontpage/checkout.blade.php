@extends("layouts.index")

@section("content")
    
	<!-- BREADCRUMB -->
	<div id="breadcrumb">
		<div class="container">
			<ul class="breadcrumb">
				<li><a href="#">Home</a></li>
				<li class="active">Checkout</li>
			</ul>
		</div>
	</div>
	<!-- /BREADCRUMB -->

	<!-- section -->
	<div class="section">
		<!-- container -->
		<div class="container">
			<!-- row -->
			<div class="row">
					<div class="col-md-12">
						<div class="order-summary clearfix">
							<div class="section-title">
								<h3 class="title">Order Review</h3>
							</div>
							<table class="shopping-cart-table table">
								<thead>
									<tr>
										<th>Product</th>
										<th></th>
										<th class="text-center">Price</th>
										<th class="text-center">Quantity</th>
										<th class="text-center">Total</th>
										<th class="text-right"></th>
									</tr>
								</thead>
								<tbody>
                                    {{$total = 0}};
                                    @if(count($carts) > 0)
                                    @foreach($carts as $cart)
									<tr>
										<td class="thumb"><img src="/storage/packages/{{$cart->package->image}}" alt=""></td>
										<td class="details">
											<h3>{{$cart->package->name}}</h3>
											{{-- <ul>
												<li><span>Size: XL</span></li>
												<li><span>Color: Camelot</span></li>
											</ul> --}}
                                        </td>
                                       
                                        <td class="price text-center"><strong>Tk {{$cart->package->price}}</strong><br></td>
                                        
										<td class="qty text-center">1</td>
										<td class="total text-center"><strong class="primary-color">Tk {{$cart->total}}</strong></td>
										{{-- <td class="text-right"><button class="main-btn icon-btn"><i class="fa fa-close"></i></button></td> --}}
									</tr>
									
									{{$total += $cart->total}}
                                    @endforeach

                                    @else
                                        <td>No Package Found</td>
                                    @endif
									
								</tbody>
								<tfoot>
									<tr>
										<th class="empty" colspan="3"></th>
										<th>SUBTOTAL</th>
										<th colspan="2" class="sub-total">Tk {{$total}}</th>
									</tr>
									<tr>
										<th class="empty" colspan="3"></th>
										<th>SHIPING</th>
										<td colspan="2">Cash On Delivery</td>
									</tr>
									<tr>
										<th class="empty" colspan="3"></th>
										<th>TOTAL</th>
										<th colspan="2" class="total">Tk {{$total}}</th>
									</tr>
								</tfoot>
							</table>
							{!!Form::open(["action" => "OrderController@index","method" => "POST","class" => "pull-right"])!!}
								{{Form::submit("Place Order",["class" => "btn btn-danger"])}}
							{!!Form::close()!!}
						</div>

					</div>
			</div>
			<!-- /row -->
		</div>
		<!-- /container -->
	</div>
	<!-- /section -->

@endsection