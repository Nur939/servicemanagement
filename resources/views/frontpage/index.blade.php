@extends("layouts.index")


@section("content")
	
	<!-- HOME -->
	<div id="home">
		<!-- container -->
		<div class="container">
			<!-- home wrap -->
			<div class="home-wrap">
				<!-- home slick -->
				<div id="home-slick">
					<!-- banner -->

					@if(count($packages) > 0)
						@foreach($packages as $package)
						<div class="banner banner-1">
							<img src="/storage/packages/{{$package->image}}" style = "height:500px;" alt="">
							<div class="banner-caption text-center">
								<h1 style = "color:red;">{{$package->name}}</h1>
								<h3 class="black-color font-weak" style = "color:#fff; font-weight:bold; font-size:30px;">Tk {{$package->price}}</h3>
								<a href = "/package/show/{{$package->id}}" class="primary-btn">Show Details</a>
							</div>
						</div>
					<!-- /banner -->
						@endforeach
					@endif

					
				</div>
				<!-- /home slick -->
			</div>
			<!-- /home wrap -->
		</div>
		<!-- /container -->
	</div>
	<!-- /HOME -->

	<!-- section -->
	<div class="section">
		<!-- container -->
		<div class="container">
			<!-- row -->
			<div class="row">
				<!-- banner -->
				<div class="section-title">
					<h2 class="title">Categories</h2>
					<div class="pull-right">
						<a href="/categories" class = "btn btn-info">All Categories</a>
					</div>
				</div>
				@if(count($categories) > 0)
					@foreach($categories as $category)
						<div class="col-md-3 col-sm-6 category">
								
							<a class="banner banner-1" href="/categories/show/{{$category->id}}">
								<img src="/storage/categories/{{$category->image}}" style = "height:250px;" alt="">
								
								<div class="product-category-title">
										
										<span class="sale">{{$category->name}}</span>
									</div>
								
							</a>
						</div>
					@endforeach	
				@endif
				<!-- /banner -->

				

			</div>
			<!-- /row -->
		</div>
		<!-- /container -->
	</div>
	<!-- /section -->

	<div class="section">
		<!-- container -->
		<div class="container">
			<!-- row -->
			<div class="row">
				<!-- banner -->
				<div class="section-title">
					<h2 class="title">Services</h2>
					<div class="pull-right">
							<a href="/services" class = "btn btn-info">All Services</a>
						</div>
				</div>
				@if(count($services) > 0)
					@foreach($services as $service)
						<div class="col-md-3 col-sm-6 category">
								
							<a class="banner banner-1" href="/services/show/{{$service->id}}">
								<img src="/storage/services/{{$service->image}}" style = "height:250px;" alt="">
								
								<div class="product-category-title">
										
										<span class="sale">{{$service->name}}</span>
									</div>
								
							</a>
						</div>
					@endforeach	
				@endif
				<!-- /banner -->

				

			</div>
			<!-- /row -->
		</div>
		<!-- /container -->
	</div>
	<!-- /section -->

	<!-- section -->
	<div class="section">
		<!-- container -->
		<div class="container">
			<!-- row -->
			<div class="row">
				<!-- section-title -->
				<div class="col-md-12">
					<div class="section-title">
						<h2 class="title">Packages</h2>
						<div class="pull-right">
							<a href="/packages" class = "btn btn-info">All Packages</a>
						</div>
					</div>
				</div>
				<!-- /section-title -->

				
				

				<!-- Product Slick -->
				<div class="col-md-12 col-sm-6 col-xs-6">
					<div class="row">
						<div id="product-slick-1" class="product-slick">
							<!-- Product Single -->
							@if(count($packages) > 0)
								@foreach($packages as $package)
								<div class="product product-single">
									<div class="product-thumb">
										<a class="main-btn quick-view" href = "/package/show/{{$package->id}}"><i class="fa fa-search-plus"></i> Quick view</a>
										<img src="/storage/packages/{{$package->image}}" style = "width:100%; height:300px;" alt="">
									</div>
									<div class="product-body">
										<h3 class="product-price">Tk {{$package->price}}</h3>
										<div class="product-rating">
												@if(count($package->ratings))
												<?php
													$sum = 0;
													$count = 0;
												?>
													@foreach($package->ratings as $rating)
														<?php $sum += (int)$rating->rating; $count++; ?>	
													@endforeach
													<?php $avg = $sum / $count; $ext = 5 - $avg; ?>	
													@for($i = 0; $i < $avg; $i++)
														<i class="fa fa-star"></i>
													@endfor
													@for($i = 0; $i < $ext; $i++)
														<i class="fa fa-star-o empty"></i>
													@endfor
												@else
												@for($i = 0; $i < 5; $i++)
													<i class="fa fa-star-o empty"></i>
												@endfor
												@endif
												
										</div>
										<h2 class="product-name"><a href="/package/show/{{$package->id}}">{{$package->name}}</a></h2>
										
									</div>
								</div>
							@endforeach
							@endif	
							<!-- /Product Single -->

							
						</div>
					</div>
				</div>
				<!-- /Product Slick -->
			</div>
			<!-- /row -->
			

@endsection