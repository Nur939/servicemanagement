@extends("layouts.index")

@section("content")

    <!-- BREADCRUMB -->
	<div id="breadcrumb">
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a></li>
                    <li><a href="/categories/show/{{$category->id}}">{{$category->name}}</a></li>
                    <li><a href="/services/show/{{$service->id}}">{{$service->name}}</a></li>
                    <li class="active">{{$package->name}}</li>
                </ul>
            </div>
        </div>
        <!-- /BREADCRUMB -->
    
        <!-- section -->
        <div class="section">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!--  Product Details -->
                    <div class="product product-details clearfix">
                        <div class="col-md-6">
                            <div id="product-main-view">
                                <div class="product-view">
                                    <img src="/storage/packages/{{$package->image}}" alt="">
                                </div>
                                <div class="product-view">
                                    <img src="/storage/packages/{{$package->image}}" alt="">
                                </div>
                                <div class="product-view">
                                    <img src="/storage/packages/{{$package->image}}" alt="">
                                </div>
                                <div class="product-view">
                                    <img src="/storage/packages/{{$package->image}}" alt="">
                                </div>
                            </div>
                            <div id="product-view">
                                <div class="product-view">
                                    <img src="/storage/packages/{{$package->image}}" alt="">
                                </div>
                                <div class="product-view">
                                    <img src="/storage/packages/{{$package->image}}" alt="">
                                </div>
                                <div class="product-view">
                                    <img src="/storage/packages/{{$package->image}}" alt="">
                                </div>
                                <div class="product-view">
                                    <img src="/storage/packages/{{$package->image}}" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="product-body">
                                <h2 class="product-name">{{$package->name}}</h2>
                                <h3 class="product-price">Tk {{$package->price}}</h3>
                                <div>
                                    <div class="product-rating">
                                       
                                        @if($avg_rating > 0)

                                            @for($i = 0; $i < $avg_rating; $i++)
                                                    <i class="fa fa-star"></i>
                                            @endfor 
                                            
                                        @endif    
                                        {{--  <i class="fa fa-star-half"></i>
                                        <i class="fa fa-star-o empty"></i>  --}}
                                    </div>
                                    <p>{{$count}} Review(s)</p>
                                </div>
                                <p><strong>Availability:</strong> In Stock</p>
                                <p><strong>Brand:</strong> E-SHOP</p>
                                <p>{{$package->description}}</p>
                                <div class="product-options">
                                    
                                </div>
    
                                <div class="product-btns">
                                    <div class="qty-input">
                                        {{--  <span class="text-uppercase">QTY: </span>
                                        <input class="input" type="number">  --}}
                                    </div>
                                    <a href = "/cart/add/{{$package->id}}" class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</a>
                                    <div class="pull-right">
                                        {{-- <button class="main-btn icon-btn"><i class="fa fa-heart"></i></button> --}}
                                        {{--  <button class="main-btn icon-btn"><i class="fa fa-exchange"></i></button>  --}}
                                        {{-- <button class="main-btn icon-btn"><i class="fa fa-share-alt"></i></button> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="product-tab">
                                <ul class="tab-nav">
                                    <li class="active"><a data-toggle="tab" href="#tab1">Description</a></li>
                                    
                                    <li><a data-toggle="tab" href="#tab2">Reviews</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div id="tab1" class="tab-pane fade in active">
                                        <p>{{$package->description}}</p>
                                    </div>
                                    <div id="tab2" class="tab-pane fade in">
    
                                        <div class="row">
                                            
                                            <div class="col-md-9">
                                                <h4>Give Your Review</h4>
                                                    <div id="disqus_thread"></div>
                                                    <script>

                                                            /**
                                                            *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                                                            *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
                                                            /*
                                                            var disqus_config = function () {
                                                            this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
                                                            this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                                                            };
                                                            */
                                                            (function() { // DON'T EDIT BELOW THIS LINE
                                                            var d = document, s = d.createElement('script');
                                                            s.src = 'https://service-4.disqus.com/embed.js';
                                                            s.setAttribute('data-timestamp', +new Date());
                                                            (d.head || d.body).appendChild(s);
                                                            })();
                                                            </script>
                                                            <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                                                            
                                            </div>    
                                              
                                            <div class="col-md-3">
                                                    @if($rate > 0)
                                                    <h4>You gave 
                                                            @for($i = 0; $i < (int)$rate; $i++)
                                                                <i class = "fa fa-star" style = "color:gold;"></i>
                                                            @endfor
                                                        </h4>
                                                     @endif   
                                                <h4 class="text-uppercase">Give Your Rating</h4>
                                                
                        
                                                <form class="review-form" action = "/user/rating" method = "POST">
                                                    {{ csrf_field() }}
                                                    <div class="form-group">
                                                        <div class="input-rating">
                                                            <strong class="text-uppercase">Your Rating: </strong>
                                                            <div class="stars">
                                                                <input type="radio" id="star5" name="rating" value="5" /><label for="star5"></label>
                                                                <input type="radio" id="star4" name="rating" value="4" /><label for="star4"></label>
                                                                <input type="radio" id="star3" name="rating" value="3" /><label for="star3"></label>
                                                                <input type="radio" id="star2" name="rating" value="2" /><label for="star2"></label>
                                                                <input type="radio" id="star1" name="rating" value="1" /><label for="star1"></label>
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <input type = "hidden" name = "package" value = "{{$package->id}}">
                                                    <input type = "submit" class="primary-btn" value = "Submit">
                                                </form>
                                            </div>
                                        </div>
    
    
    
                                    </div>
                                </div>
                            </div>
                        </div>
    
                    </div>
                    <!-- /Product Details -->
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /section -->
    
        <!-- section -->
        <div class="section">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- section title -->
                    <div class="col-md-12">
                        <div class="section-title">
                            <h2 class="title">Picked For You</h2>
                        </div>
                    </div>
                    <!-- section title -->
    
                    @if(count($packages) > 0)
                        @foreach($packages as $package)
    
                    <!-- Product Single -->
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="product product-single">
                            <div class="product-thumb">
                                
                                <a class="main-btn quick-view" href = "/package/show/{{$package->id}}"><i class="fa fa-search-plus"></i> Quick view</a>
                                <img src="/storage/packages/{{$package->image}}" style = "height:250px;" alt="">
                            </div>
                            <div class="product-body">
                                <h3 class="product-price">Tk {{$package->price}}</h3>
                                <div class="product-rating">
                                        @if(count($package->ratings))
                                        <?php
                                            $sum = 0;
                                            $count = 0;
                                        ?>
                                            @foreach($package->ratings as $rating)
                                                <?php $sum += (int)$rating->rating; $count++; ?>	
                                            @endforeach
                                            <?php $avg = $sum / $count; $ext = 5 - $avg; ?>	
                                            @for($i = 0; $i < $avg; $i++)
                                                <i class="fa fa-star"></i>
                                            @endfor
                                            @for($i = 0; $i < $ext; $i++)
                                                <i class="fa fa-star-o empty"></i>
                                            @endfor
                                        @else
                                        @for($i = 0; $i < 5; $i++)
                                            <i class="fa fa-star-o empty"></i>
                                        @endfor
                                        @endif
                                        
                                </div>
                                <h2 class="product-name"><a href="/package/show/{{$package->id}}">{{$package->name}}</a></h2>
                                {{--  <div class="product-btns">
                                    <button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
                                    <button class="main-btn icon-btn"><i class="fa fa-exchange"></i></button>
                                    <button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
                                </div>  --}}
                            </div>
                        </div>
                    </div>
                        @endforeach
                    @endif
                    <!-- /Product Single -->
    
                 
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /section -->


@endsection



