@extends("layouts.index")


@section("content")

	<!-- BREADCRUMB -->
	<div id="breadcrumb">
		<div class="container">
			<ul class="breadcrumb">
				<li><a href="/">Home</a></li>
				<li class="active">Products</li>
			</ul>
		</div>
	</div>
	<!-- /BREADCRUMB -->

	<!-- section -->
	<div class="section">
		<!-- container -->
		<div class="container">
			<!-- row -->
			<div class="row">
				
				<!-- MAIN -->
				<div id="main" class="col-md-12">
					{{-- <!-- store top filter -->
					<div class="store-filter clearfix">
						<div class="pull-left">
							<div class="row-filter">
								<a href=""><i class="fa fa-th-large"></i></a>
								<a href="" class="active"><i class="fa fa-bars"></i></a>
							</div>
							<div class="sort-filter">
								<span class="text-uppercase">Sort By:</span>
								<select class="input">
										<option value="0">Position</option>
										<option value="0">Price</option>
										<option value="0">Rating</option>
									</select>
								<a href="#" class="main-btn icon-btn"><i class="fa fa-arrow-down"></i></a>
							</div>
						</div>
						
					</div> --}}
					<!-- /store top filter -->

					<!-- STORE -->
					<div id="store">
						<!-- row -->
						<div class="row">
                            <!-- Product Single -->
                            
                            @if(count($packages) > 0)
                                @foreach($packages as $package)
							<div class="col-md-4 col-sm-6 col-xs-6">
								<div class="product product-single">
									<div class="product-thumb">
										
										<a class="main-btn quick-view" href = "/package/show/{{$package->id}}"><i class="fa fa-search-plus"></i> Quick view</a>
										<img src="/storage/packages/{{$package->image}}" style = "width:100%; height:300px;" alt="">
									</div>
									<div class="product-body">
										<h3 class="product-price">TK {{$package->price}} </h3>
										<div class="product-rating">
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star-o empty"></i>
										</div>
										<h2 class="product-name"><a href="/package/show/{{$package->id}}">{{$package->name}}</a></h2>
										
									</div>
								</div>
                            </div>
                                @endforeach
                            @endif    
							<!-- /Product Single -->

							
						</div>
						<!-- /row -->
					</div>
					<!-- /STORE -->

					<!-- store bottom filter -->
					<div class="store-filter clearfix">
						
						<div class="pull-right">
							
							<ul class="store-pages">
								
								<li class="active">{{$packages->links()}}</li>
							</ul>
						</div>
					</div>
					<!-- /store bottom filter -->
				</div>
				<!-- /MAIN -->
			</div>
			<!-- /row -->
		</div>
		<!-- /container -->
	</div>
	<!-- /section -->

@endsection	

	