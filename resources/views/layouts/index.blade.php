<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

	<title>Service Management</title>

	<!-- Google font -->
	<link href="https://fonts.googleapis.com/css?family=Hind:400,700" rel="stylesheet">

	<!-- Bootstrap -->
	<link type="text/css" rel="stylesheet" href="{{asset('../css/bootstrap.min.css')}}" />

	<!-- Slick -->
	<link type="text/css" rel="stylesheet" href="{{asset('../css/slick.css')}}" />
	<link type="text/css" rel="stylesheet" href="{{asset('../css/slick-theme.css')}}" />
	<!-- nouislider -->
	<link type="text/css" rel="stylesheet" href="{{asset('../css/nouislider.min.css')}}" />

	<!-- Font Awesome Icon -->
	<link rel="stylesheet" href="{{asset('../css/font-awesome.min.css')}}">

	<!-- Custom stlylesheet -->
	<link type="text/css" rel="stylesheet" href="{{asset('../css/style.css')}}" />

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		

</head>

<body>
	<!-- HEADER -->
	<header>
		

		<!-- header -->
		<div id="header">
			<div class="container">
				<div class="pull-left">
					<!-- Logo -->
					<div class="header-logo">
						<a class="logo" href="/">
							<img src="{{asset('../img/logo.jpg')}}" style = "width:200px; height:100px;" alt = "Service Management" alt="">
						</a>
					</div>
					<!-- /Logo -->

					<!-- Search -->
					<div class="header-search">
						<form>
							{{-- <input class="input search-input" type="text" placeholder="Enter your keyword"> --}}
							<select class="input search-categories">
								<option value="0">All Categories</option>
								@if(count($categories_menu) > 0)
									@foreach($categories_menu as $category)
										<option onclick="javascript:window.location.href='/categories/show/{{$category->id}}'; return false;"
										>{{$category->name}}</option>
									@endforeach
								@endif	
							</select>
							{{-- <button class="search-btn"><i class="fa fa-search"></i></button> --}}
						</form>
					</div>
					<!-- /Search -->
				</div>
				<div class="pull-right">
					<ul class="header-btns">
						<!-- Account -->
						<li class="header-account dropdown default-dropdown">
							<div class="dropdown-toggle" role="button" data-toggle="dropdown" aria-expanded="true">
								<div class="header-btns-icon">
									<i class="fa fa-user-o"></i>
								</div>
								<strong class="text-uppercase">My Account <i class="fa fa-caret-down"></i></strong>
							</div>
							<a href="/login" class="text-uppercase">Login</a> / <a href="/register" class="text-uppercase">Join</a>
							<ul class="custom-menu">
								<li><a href="/home"><i class="fa fa-user-o"></i> My Account</a></li>
								{{-- <li><a href="#"><i class="fa fa-heart-o"></i> My Wishlist</a></li> --}}
								
								<li><a href="/order/payment"><i class="fa fa-check"></i> Checkout</a></li>
								<li><a href="/login"><i class="fa fa-unlock-alt"></i> Login</a></li>
								<li><a href="/register"><i class="fa fa-user-plus"></i> Create An Account</a></li>
							</ul>
						</li>
						<!-- /Account -->

						<!-- Cart -->
						<li class="header-cart dropdown default-dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
								<div class="header-btns-icon">
									<i class="fa fa-shopping-cart"></i>
									<span class="qty">@if(isset($carts))
										{{$pck = count($carts)}}
										@endif
									</span>
								</div>
								<strong class="text-uppercase">My Cart:</strong>
								<br>
								<span>
										<?php $price = 0 ?>
										@if(isset($carts) && isset(Auth::user()->id))
										@if(count($carts) > 0)
											<?php $price = 0; ?>
											@foreach($carts as $cart)
												<?php $price += $cart->total; ?>
												
											@endforeach
										@endif
										@endif
										Tk {{$price}}	
								</span>
							</a>
							<div class="custom-menu">
								<div id="shopping-cart">
									<div class="shopping-cart-list">
										@if(isset($carts) && isset(Auth::user()->id))
										@if(count($carts) > 0)
											<?php $pck = count($carts); ?>
											<?php $price = 0; ?>
											@foreach($carts as $cart)
												<?php $price += $cart->total; ?>
												
											@endforeach
											<table class = "table">
											<div class="product-thumb">
												<tr>
													<th>Total Packages</th>
													<td>{{$pck}}</td>
												</tr>
											</div>
											<div class="product-body">
												<tr>
													<th>Total Price</th>
													<td>Tk {{$price}}</td>
												</tr>
											</div>
										</table>
										@else
											<p>No Packages Added</p>
										@endif
										@endif
										</div>
										
									<div class="shopping-cart-btns">
										<a href = "/cart" class="main-btn">View Cart</a>
										<a class="primary-btn" href = "/order/payment">Checkout <i class="fa fa-arrow-circle-right"></i></a>
									</div>
								</div>
							</div>
						</li>
						<!-- /Cart -->

						<!-- Mobile nav toggle-->
						<li class="nav-toggle">
							<button class="nav-toggle-btn main-btn icon-btn"><i class="fa fa-bars"></i></button>
						</li>
						<!-- / Mobile nav toggle -->
					</ul>
				</div>
			</div>
			<!-- header -->
		</div>
		<!-- container -->
	</header>
	<!-- /HEADER -->

	<!-- NAVIGATION -->
	<div id="navigation">
		<!-- container -->
		<div class="container">
			<div id="responsive-nav">
				<!-- category nav -->
				<div class="category-nav {{!Request::is('/') ? 'show-on-click' : ''}}">
					<span class="category-header text-center">Service Management</span>
				</div>
				<!-- /category nav -->

				<!-- menu nav -->
				<div class="menu-nav">
					<span class="menu-header">Menu <i class="fa fa-bars"></i></span>
					<ul class="menu-list">
						<li><a href="/">Home</a></li>
						<li class="dropdown mega-dropdown"><a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Category <i class="fa fa-caret-down"></i></a>
							<div class="custom-menu">
								<div class="row">
									<div class="col-md-4">
										<ul class="list-links">
											<li>
												<h3 class="list-links-title">Categories</h3></li>
												@foreach($categories_menu as $category)
											<li><a href="/categories/show/{{$category->id}}">{{$category->name}}</a></li>
											@endforeach
											
										</ul>
										<hr class="hidden-md hidden-lg">
									</div>
									<div class="col-md-4">
										<ul class="list-links">
											<li>
												<h3 class="list-links-title">Categories</h3></li>
												@foreach($categories_menu as $category)
												<li><a href="/categories/show/{{$category->id}}">{{$category->name}}</a></li>
												@endforeach
										</ul>
										<hr class="hidden-md hidden-lg">
									</div>
									<div class="col-md-4">
										<ul class="list-links">
											<li>
												<h3 class="list-links-title">Categories</h3></li>
												@foreach($categories_menu as $category)
												<li><a href="/categories/show/{{$category->id}}">{{$category->name}}</a></li>
												@endforeach
										</ul>
									</div>
								</div>
								<div class="row hidden-sm hidden-xs">
									<div class="col-md-12">
										<hr>
										<a class="banner banner-1" href="#">
												@foreach($categories_menu as $category)
													<img src="/storage/categories/{{$category->image}}" style = "width:100%;height:200px;" alt="">
													<?php break; ?>
												@endforeach
											
											<div class="banner-caption text-center">
												<h2 class="white-color">NEW COLLECTION</h2>
												<h3 class="white-color font-weak">HOT DEAL</h3>
											</div>
										</a>
									</div>
								</div>
							</div>
						</li>
						<li class="dropdown mega-dropdown"><a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Service <i class="fa fa-caret-down"></i></a>
							<div class="custom-menu">
								<div class="row">
									<div class="col-md-4">
										<ul class="list-links">
											<li>
												<h3 class="list-links-title">Services</h3></li>
												@foreach($services_menu as $service)
											<li><a href="/services/show/{{$service->id}}">{{$service->name}}</a></li>
											@endforeach
											
										</ul>
										<hr class="hidden-md hidden-lg">
									</div>
									<div class="col-md-4">
										<ul class="list-links">
											<li>
												<h3 class="list-links-title">Services</h3></li>
												@foreach($services_menu as $service)
												<li><a href="/services/show/{{$service->id}}">{{$service->name}}</a></li>
												@endforeach
										</ul>
										<hr class="hidden-md hidden-lg">
									</div>
									<div class="col-md-4">
										<ul class="list-links">
											<li>
												<h3 class="list-links-title">Services</h3></li>
												@foreach($services_menu as $service)
												<li><a href="/services/show/{{$service->id}}">{{$service->name}}</a></li>
												@endforeach
										</ul>
									</div>
								</div>
								<div class="row hidden-sm hidden-xs">
									<div class="col-md-12">
										<hr>
										<a class="banner banner-1" href="#">
												@foreach($services_menu as $service)
													<img src="/storage/services/{{$service->image}}" style = "width:100%;height:200px;" alt="">
													<?php break; ?>
												@endforeach
											
											<div class="banner-caption text-center">
												<h2 class="white-color">NEW COLLECTION</h2>
												<h3 class="white-color font-weak">HOT DEAL</h3>
											</div>
										</a>
									</div>
								</div>
							</div>
						</li>
						<li class="dropdown mega-dropdown"><a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Package <i class="fa fa-caret-down"></i></a>
							<div class="custom-menu">
								<div class="row">
									<div class="col-md-4">
										<ul class="list-links">
											<li>
												<h3 class="list-links-title">Packages</h3></li>
												@foreach($packages_menu as $package)
											<li><a href="/package/show/{{$package->id}}">{{$package->name}}</a></li>
											@endforeach
											
										</ul>
										<hr class="hidden-md hidden-lg">
									</div>
									<div class="col-md-4">
										<ul class="list-links">
											<li>
												<h3 class="list-links-title">Packages</h3></li>
												@foreach($packages_menu as $package)
												<li><a href="/package/show/{{$package->id}}">{{$package->name}}</a></li>
												@endforeach
										</ul>
										<hr class="hidden-md hidden-lg">
									</div>
									<div class="col-md-4">
										<ul class="list-links">
											<li>
												<h3 class="list-links-title">Packages</h3></li>
												@foreach($packages_menu as $package)
												<li><a href="/package/show/{{$package->id}}">{{$package->name}}</a></li>
												@endforeach
										</ul>
									</div>
								</div>
								<div class="row hidden-sm hidden-xs">
									<div class="col-md-12">
										<hr>
										<a class="banner banner-1" href="#">
												@foreach($packages_menu as $package)
													<img src="/storage/packages/{{$package->image}}" style = "width:100%;height:200px;" alt="">
													<?php break; ?>
												@endforeach
											
											<div class="banner-caption text-center">
												<h2 class="white-color">NEW COLLECTION</h2>
												<h3 class="white-color font-weak">HOT DEAL</h3>
											</div>
										</a>
									</div>
								</div>
							</div>
						</li>
						
						@auth
                        	<li class = "pull-right"><a href="{{route('home')}}">Dashboard</a></li>
                    	@else
							<li class = "pull-right"><a href="{{route('register')}}">Register</a></li>
							<li class = "pull-right"><a href="{{route('login')}}">Login</a></li>
                    	@endauth
						
					</ul>
				</div>
				<!-- menu nav -->
			</div>
		</div>
		<!-- /container -->
	</div>
	<!-- /NAVIGATION -->
		<div class="container">
			@include("inc.messages")
		</div>
		@yield("content")


	<!-- FOOTER -->
	<footer id="footer" class="section section-grey">
		<!-- container -->
		<div class="container">
			<!-- row -->
			<div class="row">
				<!-- footer widget -->
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="footer">
						<!-- footer logo -->
						<div class="footer-logo">
							<a class="logo" href="#">
		            <img src="{{asset('../img/logo.jpg')}}" style = "width:200px; height:100px;" alt="">
		          </a>
						</div>
						<!-- /footer logo -->

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna</p>

						<!-- footer social -->
						{{-- <ul class="footer-social">
							<li><a href="#"><i class="fa fa-facebook"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#"><i class="fa fa-instagram"></i></a></li>
							<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
							<li><a href="#"><i class="fa fa-pinterest"></i></a></li>
						</ul> --}}
						<!-- /footer social -->
					</div>
				</div>
				<!-- /footer widget -->

				<!-- footer widget -->
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="footer">
						<h3 class="footer-header">My Account</h3>
						<ul class="list-links">
							<li><a href="/home">My Account</a></li>
							{{-- <li><a href="#">My Wishlist</a></li> --}}
							
							<li><a href="/order/payment">Checkout</a></li>
							<li><a href="/login">Login</a></li>
						</ul>
					</div>
				</div>
				<!-- /footer widget -->

				<div class="clearfix visible-sm visible-xs"></div>

				<!-- footer widget -->
				{{-- <div class="col-md-3 col-sm-6 col-xs-6">
					<div class="footer">
						{{-- <h3 class="footer-header">Customer Service</h3>
						<ul class="list-links">
							<li><a href="#">About Us</a></li>
							<li><a href="#">Shiping & Return</a></li>
							<li><a href="#">Shiping Guide</a></li>
							<li><a href="#">FAQ</a></li>
						</ul> --}}
					{{-- </div>
				</div> --}}
				<!-- /footer widget -->

				<!-- footer subscribe -->
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="footer">
						<h3 class="footer-header">Stay Connected</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
						{{-- <form>
							<div class="form-group">
								<input class="input" placeholder="Enter Email Address">
							</div>
							<button class="primary-btn">Join Newslatter</button>
						</form> --}}
					</div>
				</div>
				<!-- /footer subscribe -->
			</div>
			<!-- /row -->
			
			<!-- row -->
			<div class="row">
					
				<div class="col-md-8 col-md-offset-2 text-center">
						<hr>
					<!-- footer copyright -->
					<div class="footer-copyright">
						<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
						Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Developed and Maintained by <a href="https://www.nuraalam.com" target="_blank">Nur A Alam</a>
						<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
					</div>
					<!-- /footer copyright -->
				</div>
			</div>
			<!-- /row -->
		</div>
		<!-- /container -->
	</footer>
	<!-- /FOOTER -->

	<!-- jQuery Plugins -->
	<script src="{{asset('../js/jquery.min.js')}}"></script>
	<script src="{{asset('../js/bootstrap.min.js')}}"></script>
	<script src="{{asset('../js/slick.min.js')}}"></script>
	<script src="{{asset('../js/nouislider.min.js')}}"></script>
	<script src="{{asset('../js/jquery.zoom.min.js')}}"></script>
	<script src="{{asset('../js/main.js')}}"></script>

</body>

</html>