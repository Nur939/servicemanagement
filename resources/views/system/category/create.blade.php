@extends("layouts.admin")

@section("page_title","Category")

@section("x_title","Create a category")



@section("content")

    <p>
    {!!Form::open(["action" => "CategoryController@store","method" => "POST","enctype" => "multipart/form-data"])!!}
        <div class="form-group">
            {{Form::label("name","Name")}}
            {{Form::text("name","",["class" => "form-control","placeholder" => "Category Name"])}}
        </div>
        <div class="form-group">
                {{Form::label("image","Category Image")}}
                <p>{{Form::file("category_image")}}</p>
        </div>
        <div class="form-group">
            {{Form::submit("Create",["class" => "btn btn-success"])}}
        </div>
    {!!Form::close()!!}
    </p>
@endsection