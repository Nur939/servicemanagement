@extends("layouts.admin")

@section("page_title","Category")

@section("x_title","Edit category")

@section("content")

    {!!Form::open(["action" => ["CategoryController@update",$category->id],"method" => "POST","enctype" => "multipart/form-data"])!!}
        <div class="form-group">
            {{Form::label("name","Name")}}
            {{Form::text("name",$category->name,["class" => "form-control","placeholder" => "Category Name"])}}
        </div>
        <div class="form-group">
            
        </div>
        <div class="form-group">
                {{Form::label("image","Category Image")}}
                <img src="/storage/categories/{{$category->image}}" style = "width:20%;" alt="">
                {{Form::file("category_image")}}
        </div>
        <div class="form-group">
            {{Form::hidden("_method","PUT")}}
            {{Form::submit("Update",["class" => "btn btn-success"])}}
        </div>
    {!!Form::close()!!}
@endsection