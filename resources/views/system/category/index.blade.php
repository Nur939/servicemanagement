@extends("layouts.admin")

@section("page_title","All Categories")

@section("x_title")
    <a href="/system/category/create" class = "btn btn-success">Add New Category</a>
@endsection

@section("content")
    
   @if(count($categories) > 0)
    
        <table class = "table table-striped">
          <thead>  
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Image</th>
                <th></th>
                <th></th>
            </tr>
        </thead> 

        <tbody>
            @foreach($categories as $category)
              <tr>  
                <td width = "20%">{{$category->id}}</td>
                <td width = "20%">{{$category->name}}</td>
                <td width = "20%"><img src="/storage/categories/{{$category->image}}" style = "width:20%;" alt=""></td>
                <td width = "20%"><a href="/system/category/{{$category->id}}/edit" class = "btn btn-info">Edit</a></td>
                <td width = "20%">
                    {!!Form::open(["action" => ["CategoryController@destroy",$category->id],"method" => "POST", "class" => "btn btn-danger","class" => "float-right"])!!}
                        {{Form::submit("Delete",["class" => "btn btn-danger"])}}
                        {{Form::hidden("_method","DELETE")}}
                    {!!Form::close()!!}
                </td>
              </tr>  
            @endforeach
            </tbody>  
            
        </table>
       <span class = "float-right">{{$categories->links()}}</span>
   @else
        <p>No categories Found</p>
   @endif
@endsection