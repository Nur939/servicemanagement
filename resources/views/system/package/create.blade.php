@extends("layouts.admin")

@section("page_title","Package")

@section("x_title","Create a package")

@section("content")
    

    <p>
            {!!Form::open(["action" => "PackagesController@store","method" => "POST","enctype" => "multipart/form-data"])!!}
                <div class="form-group">
                    {{Form::label("name","Name")}}
                    {{Form::text("Name","",["class" => "form-control","placeholder" => "Package Name"])}}
                </div>

                <div class="form-group">
                        
                    {{Form::label("ser","Service")}}
                    
                    {{Form::select("service",$services,null,["class" => "form-control", "placeholder" => "Select Category"])}}
          
                </div>

                <div class="form-group">
                    {{Form::label("price","Price")}}
                    {{Form::text("price","",["class" => "form-control","placeholder" => "Price"])}}
                </div>

                <div class="form-group">
                    {{Form::label("desc","Description")}}
                    {{Form::textarea("description","",["id" => "article-ckeditor","class" => "form-control","placeholder" => "Description"])}}
                </div>

               
                
                <div class="form-group">
                        {{Form::label("image","Package Image")}}
                        <p>{{Form::file("Package_Image")}}</p>
                </div>
                <div class="form-group">
                    {{Form::submit("Create",["class" => "btn btn-success"])}}
                </div>
            {!!Form::close()!!}
            </p>
@endsection