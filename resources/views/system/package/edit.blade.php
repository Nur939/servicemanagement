@extends("layouts.admin")

@section("page_title","Package")

@section("x_title","Edit Package")

@section("content")
    

    {!!Form::open(["action" => ["PackagesController@update",$package->id],"method" => "POST","enctype" => "multipart/form-data"])!!}
        <div class="form-group">
            {{Form::label("name","Name")}}
            {{Form::text("name",$package->name,["class" => "form-control","placeholder" => "Category Name"])}}
        </div>
        <div class="form-group">
                        
                {{Form::label("cat","Service")}}
                
                {{Form::select("service",$select,$package->service_id,["class" => "form-control", "placeholder" => "Select Category"])}}
      
            </div>
            <div class="form-group">
                    {{Form::label("price","Price")}}
                    {{Form::text("price",$package->price,["class" => "form-control","placeholder" => "Price"])}}
                </div>

                <div class="form-group">
                    {{Form::label("desc","Description")}}
                    {{Form::textarea("description",$package->description,["id" => "article-ckeditor","class" => "form-control","placeholder" => "Description"])}}
                </div>
        <div class="form-group">
                {{Form::label("image","Package Image")}}
                <img src="/storage/packages/{{$package->image}}" style = "width:20%;" alt="">
                {{Form::file("package_image")}}
        </div>
        <div class="form-group">
            {{Form::hidden("_method","PUT")}}
            {{Form::submit("Update",["class" => "btn btn-success"])}}
        </div>
    {!!Form::close()!!}
@endsection