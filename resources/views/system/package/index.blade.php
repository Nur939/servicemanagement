@extends("layouts.admin")

@section("page_title","All Packages")

@section("x_title")
    <a href="/system/package/create" class = "btn btn-success">Add package</a>
@endsection

@section("content")
    
   

    @if(count($packages) > 0)
        <table class = "table table-striped">
          <thead>  
            <tr>
                <th>ID</th>
                <th>Service</th>
                <th>Name</th>
                <th>Description</th>
                <th>Price</th>
                <th>Image</th>
                <th></th>
                <th></th>
            </tr>
        </thead> 

        <tbody>

            @foreach($packages as $package)
                <tr>
                    <td width = "12%">{{$package->id}}</td>
                    <td width = "12%">{{$package->service_id}}</td>
                    <td width = "12%"><a href = "/system/package/{{$package->id}}" style = "color:blue;">{{$package->name}}</a></td>
                    <td width = "12%">{!!$package->description!!}</td>
                    <td width = "12%">{{$package->price}}</td>
                    <td width = "12%"><img src="/storage/packages/{{$package->image}}" style = "width:20%;" alt=""></td>
                    <td width = "12%"><a href="/system/package/{{$package->id}}/edit" class = "btn btn-info">Edit</a></td>
                    <td width = "12%">
                        {!!Form::open(["action" => ["PackagesController@destroy",$package->id],"method" => "POST", "class" => "btn btn-danger","class" => "float-right"])!!}
                            {{Form::submit("Delete",["class" => "btn btn-danger"])}}
                            {{Form::hidden("_method","DELETE")}}
                        {!!Form::close()!!}
                    </td>
                </tr>
            @endforeach
            </tbody> 
            
        </table>
    @endif
    <span class = "float-right">{{$packages->links()}}</span>
@endsection