@extends("layouts.admin")

@section("page_title","Package")


@section("content")
    <div class="row">
        <div class="col-md-4 col-sm-4">
            <img src="/storage/packages/{{$package->image}}" style = "width:100%;" alt="">
        </div>
        <div class="col-md-8 col-sm-8">
            <h1>Package ID - {{$package->id}}</h1>
            <h1>Package Name - {{$package->name}}</h1>
            <h1>Service ID - {{$package->service_id}}</h1>
            <h1>Package Description - {{$package->description}}</h1>
            <h1>Package Price - {{$package->price}}</h1>

        </div>
    </div>
    <div id="disqus_thread"></div>
<script>

/**
*  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
*  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
/*
var disqus_config = function () {
this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
};
*/
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://service-4.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>


@endsection