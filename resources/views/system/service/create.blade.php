@extends("layouts.admin")

@section("page_title","Service")

@section("x_title","Create a Service")

@section("content")
   

    <p>
            {!!Form::open(["action" => "ServicesController@store","method" => "POST","enctype" => "multipart/form-data"])!!}
                <div class="form-group">
                    {{Form::label("name","Name")}}
                    {{Form::text("name","",["class" => "form-control","placeholder" => "Service Name"])}}
                </div>

                <div class="form-group">
                        
                        {{Form::label("cat","Category")}}
                        
                        {{Form::select("category",$select,null,["class" => "form-control", "placeholder" => "Select Category"])}}
              
                    </div>
                
                <div class="form-group">
                        {{Form::label("image","Service Image")}}
                        <p>{{Form::file("service_image")}}</p>
                </div>
                <div class="form-group">
                    {{Form::submit("Create",["class" => "btn btn-success"])}}
                </div>
            {!!Form::close()!!}
            </p>
@endsection