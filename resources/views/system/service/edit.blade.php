@extends("layouts.admin")

@section("page_title","Service")

@section("x_title","Edit Service")



@section("content")
   

    {!!Form::open(["action" => ["ServicesController@update",$service->id],"method" => "POST","enctype" => "multipart/form-data"])!!}
        <div class="form-group">
            {{Form::label("name","Name")}}
            {{Form::text("name",$service->name,["class" => "form-control","placeholder" => "Category Name"])}}
        </div>
        <div class="form-group">
                        
                {{Form::label("cat","Category")}}
                
                {{Form::select("service",$select,$service->category_id,["class" => "form-control", "placeholder" => "Select Category"])}}
      
            </div>
        <div class="form-group">
                {{Form::label("image","Category Image")}}
                <img src="/storage/services/{{$service->image}}" style = "width:20%;" alt="">
                {{Form::file("service_image")}}
        </div>
        <div class="form-group">
            {{Form::hidden("_method","PUT")}}
            {{Form::submit("Update",["class" => "btn btn-success"])}}
        </div>
    {!!Form::close()!!}
@endsection