@extends("layouts.admin")

@section("page_title","All Services")

@section("x_title")
    <a href="/system/service/create" class = "btn btn-success">Add New Service</a>
@endsection


@section("content")
    
    

    @if(count($services) > 0)
        <table class = "table table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Category</th>
                    <th>Name</th>
                    <th>Image</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>   

            <tbody>
            @foreach($services as $service)
                <tr>
                    <td width = "16%">{{$service->id}}</td>
                    <td width = "16%">{{$service->category_id}}</td>
                    <td width = "16%">{{$service->name}}</td>
                    <td width = "16%"><img src="/storage/services/{{$service->image}}" style = "width:30%;" alt=""></td>
                    <td width = "16%"><a href="/system/service/{{$service->id}}/edit" class = "btn btn-info">Edit</a></td>
                <td width = "16%">
                    {!!Form::open(["action" => ["ServicesController@destroy",$service->id],"method" => "POST", "class" => "btn btn-danger","class" => "float-right"])!!}
                        {{Form::submit("Delete",["class" => "btn btn-danger"])}}
                        {{Form::hidden("_method","DELETE")}}
                    {!!Form::close()!!}
                </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <p>No Services Found</p>    
    @endif
    <span class = "float-right">{{$services->links()}}</span>

@endsection