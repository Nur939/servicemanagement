@extends("layouts.user")

@section("page_title","User Orders")

@section("x_title")
    <h1>All Orders Information</h1>
@endsection


@section("content")
   
@if(count($orders) > 0)
<table class = "table table-striped">
  <thead>  
    <tr>
        <th>ID</th>
        <th>User Name</th>
        <th>Order ID</th>
        <th>Order Date</th>
        <th>Current Status</th>
        <th>Options</th>
        <th>Status</th>
    </tr>
</thead> 

<tbody>

    @foreach($orders as $order)
        <tr>
            <td width = "12%">{{$order->user_id}}</td>
            <td width = "12%">{{$order->name}}</td>
            <td width = "12%"><a href = "/orderinfo/{{$order->id}}" class = "text-primary">{{$order->order_randid}}</a></td>
            <td width = "12%">{{$order->date }}</td>
            <td width = "12%">{{$order->status}}</td>
            <?php $status = 1; ?>
            @if($order->status == "pending")
                <?php $options = [0 => "confirm"]; ?>        
            @else
                <?php $status = 0; ?>    
            @endif
            <td width = "12%">
            {!!Form::open(["action" => "HomeController@confirm","method" => "POST","onsubmit" => "return confirm('Are you sure?')"])!!}
                {{Form::hidden("order_id",$order->id)}}
                @if($status == 1)
                {{Form::select("options",$options)}}</td>  
                <td width = "12%">{{Form::submit("Confirm",["class" => "btn btn-info"])}}</td>
                @elseif($status == 0)
                    <td  style = "color:green;">Success</td>
                   
              
                
                @endif 
            {!!Form::close()!!}
           
        </tr>
    @endforeach
    </tbody> 
    
</table>
@else   
    <p>No Carts Found</p>
@endif

@endsection