<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource("/","MainController");

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource("/system/category","CategoryController");

Route::resource("/system/service","ServicesController");

Route::resource("/system/package","PackagesController");

Route::get("/packages","PackagFrontController@index");

Route::get("/categories","CategoryFrontController@index");

Route::get("/services","ServiceFrontController@index");

Route::get("/services/show/{id}","ServiceFrontController@show");

Route::get("/categories/show/{id}","CategoryFrontController@show");

Route::get("/package/show/{id}","packagFrontController@show");

Route::get("/cart/add/{id}","CartController@add");

Route::get("/cart","CartController@show");

Route::get("/order/payment","CartController@order");

Route::get("/user/cart","AdminCartController@index");

Route::get("/user/cartinfo/{id}","AdminCartController@show");

// Route::prefix('admin')->group(function() {
//     Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    
//     Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    
    Route::get('/admin', 'AdminController@index')->name('admin.dashboard');
  // });

Route::post("/order/confirm","OrderController@index");  

Route::get("/user/order","AdminOrderController@index");

Route::get("/user/orderinfo/{id}","AdminOrderController@show");

Route::post("/user/select","AdminOrderController@confirm");

Route::get("/orders","HomeController@orders");

Route::post("order/select","HomeController@confirm");

Route::get("/orderinfo/{id}","HomeController@show");

Route::post("/user/rating","RatingController@rating");
