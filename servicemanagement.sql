-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 27, 2018 at 12:44 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `servicemanagement`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', '$2y$10$aN8c5N0Pnui.vrg8x1qIKOJ1l.ALDBUsKYPfevFk8M8pC8APPCrLG', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'admin', 'admins@gmail.com', '$2y$10$aN8c5N0Pnui.vrg8x1qIKOJ1l.ALDBUsKYPfevFk8M8pC8APPCrLG', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `total` decimal(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `carts`
--

INSERT INTO `carts` (`id`, `user_id`, `package_id`, `total`, `created_at`, `updated_at`) VALUES
(2, 2, 8, '1000.00', '2018-03-27 08:12:15', '2018-03-27 08:12:15');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(160) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `image`, `created_at`, `updated_at`) VALUES
(31, 'Computer', 'index_1520419857.jpg', '2018-03-04 00:06:03', '2018-03-07 04:50:58'),
(32, 'Category2', 'home_it_sectionbg1_1520757184.jpg', '2018-03-11 02:33:04', '2018-03-11 02:33:04'),
(33, 'Category3', 'home_it_sectionbg4_1520757206.jpg', '2018-03-11 02:33:26', '2018-03-11 02:33:26'),
(34, 'Category4', 'Computer_1520936574.jpg', '2018-03-13 04:16:13', '2018-03-13 04:22:54');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `order_id`, `package_id`, `created_at`, `updated_at`) VALUES
(13, 12, 7, '2018-03-25 01:55:26', '2018-03-25 01:55:26'),
(14, 13, 7, '2018-03-25 08:53:24', '2018-03-25 08:53:24'),
(15, 14, 7, '2018-03-25 08:56:03', '2018-03-25 08:56:03'),
(16, 15, 7, '2018-03-25 09:09:29', '2018-03-25 09:09:29'),
(17, 15, 9, '2018-03-25 09:09:29', '2018-03-25 09:09:29'),
(18, 16, 8, '2018-03-25 09:20:40', '2018-03-25 09:20:40'),
(19, 17, 9, '2018-03-25 10:59:17', '2018-03-25 10:59:17'),
(20, 18, 7, '2018-03-25 10:59:52', '2018-03-25 10:59:52'),
(21, 19, 7, '2018-03-27 07:50:02', '2018-03-27 07:50:02');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_02_25_083422_create_categories_table', 1),
(4, '2018_02_25_083439_create_services_table', 1),
(5, '2018_02_25_083459_create_packages_table', 1),
(7, '2018_03_20_075421_create_carts_table', 2),
(8, '2018_03_25_055536_create_orders_table', 3),
(9, '2018_03_25_055656_create_items_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_randid` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `order_randid`, `status`, `created_at`, `updated_at`) VALUES
(13, 2, '1565175663', 'approved', '2018-03-25 08:53:24', '2018-03-25 10:09:08'),
(14, 2, '548942348', 'delivered', '2018-03-25 08:56:03', '2018-03-25 10:09:13'),
(15, 2, '0', 'confirm', '2018-03-25 09:09:29', '2018-03-25 10:57:59'),
(16, 2, 'flVmI1Sa1ph9TJ4', 'delivered', '2018-03-25 09:20:40', '2018-03-25 10:31:41'),
(17, 2, 'ZMLYfmscP70DXUC', 'confirm', '2018-03-25 10:59:17', '2018-03-25 10:59:32'),
(18, 2, 'q2kl8Ag5KH2Ry79', 'confirm', '2018-03-25 10:59:52', '2018-03-25 11:00:08'),
(19, 2, '29bc0TeE3zaLPDU', 'pending', '2018-03-27 07:50:02', '2018-03-27 07:50:02');

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_id` int(11) NOT NULL,
  `name` varchar(160) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(16,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `service_id`, `name`, `image`, `description`, `price`, `created_at`, `updated_at`) VALUES
(6, 12, 'Repair', '5775303_rd_1520924725.jpg', 'This is package', '100.00', '2018-03-04 00:50:08', '2018-03-13 01:05:25'),
(7, 13, 'Package1', 'home_it_zoombox3_1520762296.jpg', 'Lorem ipsum dolor sit amet Lorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit amet', '500.00', '2018-03-11 03:58:16', '2018-03-11 03:58:16'),
(8, 14, 'Package2', 'home_it_zoombox2_1520762321.jpg', 'Lorem ipsum dolor sit amet Lorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit amet', '1000.00', '2018-03-11 03:58:41', '2018-03-11 03:58:41'),
(9, 14, 'Package4', 'banner03_1520924610.jpg', 'Lorem ipsum dolor sit Lorem ipsum dolor sitLorem ipsum dolor sitLorem ipsum dolor sitLorem ipsum dolor sitLorem ipsum dolor sitLorem ipsum dolor sitLorem ipsum dol', '5000.00', '2018-03-11 04:26:19', '2018-03-13 01:03:31');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(160) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `category_id`, `name`, `image`, `created_at`, `updated_at`) VALUES
(12, 31, 'Motherboard', 'best_motherboard_gigabyte_ab350_gaming_3_1520144911.jpg', '2018-03-04 00:28:31', '2018-03-04 03:49:02'),
(13, 32, 'Service1', 'home_it_offer3_1520762222.png', '2018-03-11 03:57:02', '2018-03-11 03:57:02'),
(14, 34, 'Service3', 'home_it_zoombox2_1520762246.jpg', '2018-03-11 03:57:26', '2018-03-14 02:21:59'),
(15, 33, 'Service4', 'mirrorless-camera-fujifilm-x-t2-lowres-2024-570x380_1520937088.jpg', '2018-03-13 04:31:28', '2018-03-13 04:31:28');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `type`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@gmail.com', '$2y$10$D.iluTJbdfpV9R7a1Qd/L.gSXfbPYcaBPJEkHg.3QELIiWZJemPu.', 'admin', 'MTWkvbqYU1fZshGk1XT0JqJ0lb8np7xfLDMpSI0im4Z5KZbTrVfzZHMFnzXF', '2018-02-27 00:15:35', '2018-02-27 00:15:35'),
(2, 'Def', 'def@gmail.com', '$2y$10$1R1oVOeAIXIPYcLWJrrfLOMf9ei4gaYpAEJkvz/lB0UQXLiqVuRP.', 'User', 'uPpC5OmFpz9Inwmr9kuKiCblxj4XAyZWZD2BpaMrrr8bXwkilVyR05SOL2e1', '2018-03-15 04:18:50', '2018-03-15 04:18:50'),
(3, 'a', 'a@gmail.com', '$2y$10$.T9ghog8mEB5YHYSzrD5COshQMoWrJJlSS6nKTYbVbS0oSu6nawmK', 'User', '2IlqjyYs9edSmhjuNU68sV4WgIvQsjWqIsw8kLpJArKPxuylIYgFn7kB9Z5U', '2018-03-15 04:28:41', '2018-03-15 04:28:41');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
